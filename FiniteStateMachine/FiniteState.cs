﻿namespace RPGTest.Framework.FSM
{
    using System;

    /// <summary>
    /// 有限状态
    /// </summary>
    public abstract class FiniteState
    {
        #region 属性
        /// <summary>
        /// 拥有者
        /// </summary>
        protected readonly UnityEngine.Object owner;
        #endregion

        #region 构造函数
        public FiniteState(UnityEngine.Object @object)
        {
            owner = @object;
        }
        #endregion

        #region 方法
        /// <summary>
        /// 转换状态
        /// </summary>
        /// <returns>下一个状态</returns>
        public abstract Type TransitionState();
        /// <summary>
        /// 进入状态执行一次
        /// </summary>
        public abstract void OnEnter();
        /// <summary>
        /// 保持状态每帧刷新
        /// </summary>
        public abstract void OnUpdate();
        /// <summary>
        /// 退出状态执行一次
        /// </summary>
        public abstract void OnExit();
        #endregion
    }
}