﻿namespace RPGTest.Character.Enemy
{
    using System;
    using UnityEngine;
    using Framework.FSM;

    #region 有限状态机
    public class EnemyFSM : MonoBehaviour
    {
        FiniteStateMachine fsm;
        EnemyController enemyController;

        private void Awake()
        {
            enemyController = GetComponent<EnemyController>();
            fsm = new FiniteStateMachine();
            fsm.AddState(typeof(GuardState), new GuardState(enemyController));
            fsm.AddState(typeof(PatrolState), new PatrolState(enemyController));
            fsm.AddState(typeof(ChaseState), new ChaseState(enemyController));
            fsm.AddState(typeof(DeathState), new DeathState(enemyController));
        }

        private void OnEnable()
        {
            fsm.StartFSM(typeof(PatrolState));
        }

        private void Update()
        {
            fsm.StayFSM();
        }

        private void OnDisable()
        {
            fsm.StopFSM();
        }
    }
    #endregion

    #region 有限状态
    /// <summary>
    /// 守备状态
    /// </summary>
    public class GuardState : FiniteState
    {
        protected readonly EnemyController enemyController;
        public GuardState(EnemyController controller) : base(controller)
        {
            this.enemyController = controller as EnemyController;
        }

        public override Type TransitionState()
        {
            Debug.Log("守备状态检查");
            return null;
        }

        public override void OnEnter()
        {
            Debug.Log("进入守备状态");
        }

        public override void OnExit()
        {
            Debug.Log("退出守备状态");
        }

        public override void OnUpdate()
        {
            Debug.Log("守备状态中");
        }
    }

    /// <summary>
    /// 巡逻状态
    /// </summary>
    public class PatrolState : FiniteState
    {
        protected readonly EnemyController enemyController;

        float standingTimer;  // 停留计时器

        public PatrolState(EnemyController controller) : base(controller)
        {
            this.enemyController = controller as EnemyController;
        }

        public override Type TransitionState()
        {
            Debug.Log("巡逻状态检查");
            return null;
        }

        public override void OnEnter()
        {
            Debug.Log("进入巡逻状态");
        }

        public override void OnExit()
        {
            Debug.Log("退出巡逻状态");
        }

        public override void OnUpdate()
        {
            Debug.Log("巡逻状态中");
        }
    }

    /// <summary>
    /// 追击状态
    /// </summary>
    public class ChaseState : FiniteState
    {
        protected readonly EnemyController enemyController;
        public ChaseState(EnemyController controller) : base(controller)
        {
            this.enemyController = controller as EnemyController;
        }

        public override Type TransitionState()
        {
            Debug.Log("追击状态检查");
            return null;
        }

        public override void OnEnter()
        {
            Debug.Log("进入追击状态");
        }

        public override void OnExit()
        {
            Debug.Log("退出追击状态");
        }

        public override void OnUpdate()
        {
            Debug.Log("追击状态中");
        }
    }

    /// <summary>
    /// 死亡状态
    /// </summary>
    public class DeathState : FiniteState
    {
        protected readonly EnemyController enemyController;
        public DeathState(EnemyController controller) : base(controller)
        {
            this.enemyController = controller as EnemyController;
        }

        public override Type TransitionState()
        {
            Debug.Log("死亡状态检查");
            return null;
        }

        public override void OnEnter()
        {
            Debug.Log("进入死亡状态");
        }

        public override void OnExit()
        {
            Debug.Log("退出死亡状态");
        }

        public override void OnUpdate()
        {
            Debug.Log("进入死亡状态");
        }
    }
    #endregion
}