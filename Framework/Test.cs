namespace RPGTest.Test
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;


    /// <summary>
    /// 数据泛型类
    /// </summary>
    /// <typeparam name="T">数据类型</typeparam>
    public struct ModelValue<T>
    {
        private T mValue;
        public T MValue
        {
            get => mValue;
            set
            {
                // 触发监听委托
                if (!value.Equals(mValue)) OnValueChange?.Invoke(value);
                mValue = value;
            }
        }
        /// <summary>
        /// 监听委托
        /// </summary>
        public event Action<T> OnValueChange;
    }

    /// <summary>
    /// 测试枚举
    /// </summary>
    public enum TestE
    {
        A,
        B,
    }

    /// <summary>
    /// 测试结构体
    /// </summary>
    public struct TestS { public string name; }

    /// <summary>
    /// 测试类
    /// </summary>
    public class TestC : TestI { }

    /// <summary>
    /// 测试接口
    /// </summary>
    public interface TestI { }

    /// <summary>
    /// 测试委托
    /// </summary>
    public delegate void TestD();

    /// <summary>
    /// 测试脚本类
    /// </summary>
    public class Test : MonoBehaviour
    {
        private void Start()
        {
            // 整形
            print("整形");
            ModelValue<int> mv1 = new ModelValue<int>();
            mv1.OnValueChange += (int a) => print(a);
            mv1.MValue = 0;
            mv1.MValue = 1;
            print("-----------------------");

            // 浮点型
            print("浮点型");
            ModelValue<float> mv2 = new ModelValue<float>();
            mv2.OnValueChange += (float a) => print(a);
            mv2.MValue = 0;
            mv2.MValue = .1f;
            print("-----------------------");

            // 布尔类型
            print("布尔类型");
            ModelValue<bool> mv3 = new ModelValue<bool>();
            mv3.OnValueChange += (bool a) => print(a);
            mv3.MValue = false;
            mv3.MValue = true;
            print("-----------------------");

            // 字符型
            print("字符型");
            ModelValue<char> mv4 = new ModelValue<char>();
            mv4.OnValueChange += (char a) => print(a);
            mv4.MValue = '\0';
            mv4.MValue = 'a';
            print("-----------------------");

            // 枚举
            print("枚举");
            ModelValue<TestE> mv5 = new ModelValue<TestE>();
            mv5.OnValueChange += (TestE a) => print(a);
            mv5.MValue = TestE.A;
            mv5.MValue = TestE.B;
            print("-----------------------");

            // 结构体
            print("结构体");
            ModelValue<TestS> mv6 = new ModelValue<TestS>();
            mv6.OnValueChange += (TestS a) => print(a);
            TestS testS = new TestS();
            mv6.MValue = new TestS();
            mv6.MValue = testS;
            testS.name = "a";
            mv6.MValue = testS;
            print("-----------------------");

            // 元组
            print("元组");
            ModelValue<(int, float)> mv7 = new ModelValue<(int, float)>();
            mv7.OnValueChange += ((int, float) a) => print(a);
            mv7.MValue = (0, 0);
            mv7.MValue = (1, 0);
            print("-----------------------");

            // 类
            print("类");
            ModelValue<TestC> mv8 = new ModelValue<TestC>();
            mv8.OnValueChange += (TestC a) => print(a);
            TestC testC = new TestC();
            mv8.MValue = new TestC();
            mv8.MValue = testC;
            print("-----------------------");

            // 接口
            print("接口");
            ModelValue<TestI> mv9 = new ModelValue<TestI>();
            mv9.OnValueChange += (TestI a) => print(a);
            TestI testI = testC;
            mv9.MValue = testI;
            print("-----------------------");

            // 委托
            print("委托");
            ModelValue<TestD> mv10 = new ModelValue<TestD>();
            mv10.OnValueChange += (TestD a) => print(a);
            TestD testD = () => { };
            TestD testD1 = () => { };
            mv10.MValue = testD;
            mv10.MValue = testD1;
            print("-----------------------");

            // 数组
            print("数组");
            ModelValue<int[]> mv11 = new ModelValue<int[]>();
            mv11.OnValueChange += (int[] a) => print(a);
            int[] intA = new int[] { 1, 2 };
            mv11.MValue = new int[] { 1, 2 };
            mv11.MValue = intA;
            intA[0] = 3;
            mv11.MValue = intA;
            print("-----------------------");

            // 集合
            print("集合");
            ModelValue<List<int>> mv12 = new ModelValue<List<int>>();
            mv12.OnValueChange += (List<int> a) => print(a);
            List<int> lA = new List<int>();
            mv12.MValue = new List<int>();
            mv12.MValue = lA;
            lA.Add(1);
            mv12.MValue = lA;
            print("-----------------------");

            // 字典
            print("字典");
            ModelValue<Dictionary<int, int>> mv13 = new ModelValue<Dictionary<int, int>>();
            mv13.OnValueChange += (Dictionary<int, int> a) => print(a);
            Dictionary<int, int> dA = new Dictionary<int, int>();
            mv13.MValue = new Dictionary<int, int>();
            mv13.MValue = dA;
            dA.Add(1, 1);
            mv13.MValue = dA;
            print("-----------------------");
        }
    }

}