namespace InventorySystem.Script
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public enum ItemType
    {
        Food,
        Equipment,
        Defualt
    }

    public abstract class ItemObject : ScriptableObject
    {
        public GameObject prefab;
        public ItemType type;
        [TextArea(4, 8)] public string description;
    }
}