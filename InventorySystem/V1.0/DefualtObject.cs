namespace InventorySystem.Script
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(fileName ="New Defualt Object",menuName ="Inventory System/Items/Defualt")]
    public class DefualtObject : ItemObject
    {
        private void Awake()
        {
            type = ItemType.Defualt;
        }
    }
}