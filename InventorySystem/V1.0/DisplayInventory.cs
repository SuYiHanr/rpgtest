namespace InventorySystem.Script
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using TMPro;
    using UnityEngine;

    public class DisplayInventory : MonoBehaviour
    {
        public InventoryObject inventory;

        Dictionary<InventorySlot, GameObject> itemDisplayed = new Dictionary<InventorySlot, GameObject>();

        void Start()
        {
            CreateDisplay();
        }

        void Update()
        {
            UpdateDisplay();
        }

        private void CreateDisplay()
        {
            inventory.container.ForEach(i =>
            {
                var obj = Instantiate(i.item.prefab, Vector3.zero, Quaternion.identity, transform);
                obj.GetComponentInChildren<TextMeshProUGUI>().text = i.amount.ToString("n0");
                itemDisplayed.Add(i, obj);
            });
        }

        private void UpdateDisplay()
        {
            inventory.container.ForEach(i =>
            {
                if (itemDisplayed.ContainsKey(i)) itemDisplayed[i].GetComponentInChildren<TextMeshProUGUI>().text = i.amount.ToString("n0");
                else
                {
                    var obj = Instantiate(i.item.prefab, Vector3.zero, Quaternion.identity, transform);
                    obj.GetComponentInChildren<TextMeshProUGUI>().text = i.amount.ToString("n0");
                    itemDisplayed.Add(i, obj);
                }
            });
        }

    }
}