namespace InventorySystem.Script
{
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using UnityEngine;

    [CreateAssetMenu(fileName = "New Inventory Object", menuName = "Inventory System/Inventory")]
    public class InventoryObject : ScriptableObject, ISerializationCallbackReceiver
    {
        public string savePath;
        public List<InventorySlot> container = new List<InventorySlot>();

        ItemDatabaseObject database;

        private void OnEnable()
        {
            database = Resources.Load<ItemDatabaseObject>("Database");
        }

        public void AddItem(ItemObject itemObject, int amount)
        {
            foreach (var item in container)
            {
                if (item.item == itemObject)
                {
                    item.AddAmount(amount);
                    return;
                }
            }
            container.Add(new InventorySlot(database.GetId[itemObject], itemObject, amount));
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        public void Save()
        {
            Debug.Log(Application.persistentDataPath);
            // 将对象以便于查看的方式转换为 JSON 对象
            string saveData = JsonUtility.ToJson(this, true);
            // 创建二进制序列化对象
            BinaryFormatter bf = new BinaryFormatter();
            // 创建文件流 在公共目录上存储文件
            FileStream file = File.Create(string.Concat(Application.persistentDataPath, savePath));
            // 将 JSON 对象序列化到文件中
            bf.Serialize(file, saveData);
            // 关闭文件流
            file.Close();

        }

        /// <summary>
        /// 加载数据
        /// </summary>
        public void Load()
        {
            // 判断文件是否存在
            if (File.Exists(string.Concat(Application.persistentDataPath, savePath)))
            {
                // 创建二进制序列化对象
                BinaryFormatter bf = new BinaryFormatter();
                // 打开文件
                FileStream file = File.Open(string.Concat(Application.persistentDataPath, savePath), FileMode.Open);
                // 反序列化 JSON 对象并覆盖当前对象
                JsonUtility.FromJsonOverwrite(bf.Deserialize(file).ToString(), this);
                // 关闭文件流
                file.Close();
            }
        }

        public void OnAfterDeserialize()
        {
            for (int i = 0; i < container.Count; i++) container[i].item = database.GetItem[container[i].id];
        }

        public void OnBeforeSerialize()
        {
            //throw new System.NotImplementedException();
        }
    }

    /// <summary>
    /// 物品槽
    /// </summary>
    [System.Serializable]
    public class InventorySlot
    {
        public int id;
        public ItemObject item;
        public int amount;

        public InventorySlot(int id, ItemObject item, int amount)
        {
            this.id = id;
            this.item = item;
            this.amount = amount;
        }

        public void AddAmount(int value)
        {
            amount += value;
        }
    }
}