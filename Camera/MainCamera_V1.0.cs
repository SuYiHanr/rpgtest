using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPGTest.Camera
{
    public class MainCamera : MonoBehaviour
    {
        /// <summary>
        /// 跟随目标
        /// </summary>
        [Header("相机跟随")] public Transform follow;
        /// <summary>
        /// 看向目标
        /// </summary>
        public Transform lookAt;
        /// <summary>
        /// 相机臂长度
        /// </summary>
        [Header("相机臂"), Range(0, 10)] public float armLeghth = 5;
        /// <summary>
        /// 相机臂垂直角度
        /// </summary>
        [Range(0, 89)] public float armVerticalRadius = 45;  // 移动到90会重置旋转
        /// <summary>
        /// 相机臂水平角度
        /// </summary>
        [Range(-180, 180)] public float armHorizontalRadius = -90;
        /// <summary>
        /// 水平移动反转
        /// </summary>
        [Header("相机反转")] public bool horizontalInvert = true;
        /// <summary>
        /// 垂直移动反转
        /// </summary>
        public bool verticalInvert = true;
        /// <summary>
        /// 远近移动反转
        /// </summary>
        public bool distanceInvert = true;
        /// <summary>
        /// 相机旋转速度
        /// </summary>
        [Header("相机移动速度")] public float horizontalSpeed = 1f;
        /// <summary>
        /// 相机上下速度
        /// </summary>
        public float verticalSpeed = 1f;
        /// <summary>
        /// 相机远近速度
        /// </summary>
        public float distanceSpeed = 1f;

        private void Update()
        {
            CalculateCameraPosition();
            if (lookAt) transform.LookAt(lookAt);
            Rotation();
        }

        /// <summary>
        /// 通过计算圆上点的位置确定相机位置
        /// </summary>
        void CalculateCameraPosition()
        {
            Vector3 center = follow.position;
            // 先计算 z/y 平面相机臂的位置 计算相机偏移
            // 圆心 z 轴负方向为 0°
            float x0 = center.z;
            float y0 = center.y;
            // 下个圆的边长
            float x1 = x0 + armLeghth * Mathf.Cos(armVerticalRadius * Mathf.PI / 180);
            // 相机 y
            float y1 = y0 + armLeghth * Mathf.Sin(armVerticalRadius * Mathf.PI / 180);

            // 再计算 x/z 平面相机臂的位置 计算相机旋转范围
            // 圆心 x 轴正方向为 0°
            float x2 = center.x;
            float y2 = center.z;
            // 相机 x
            float x3 = x2 + Mathf.Abs(x1 - x0) * Mathf.Cos(armHorizontalRadius * Mathf.PI / 180);
            // 相机 z
            float y3 = y2 + Mathf.Abs(x1 - x0) * Mathf.Sin(armHorizontalRadius * Mathf.PI / 180);

            // 设置相机位置
            transform.position = new Vector3(x3, y1, y3);
        }

        /// <summary>
        /// 旋转朝向
        /// </summary>
        void Rotation()
        {
            // 左右旋转
            float horizontalRotaion = horizontalInvert ? -Input.GetAxis("Mouse X") : Input.GetAxis("Mouse X");
            armHorizontalRadius += horizontalRotaion * horizontalSpeed;
            if (armHorizontalRadius > 180 || armHorizontalRadius < -180) armHorizontalRadius = -armHorizontalRadius;
            armHorizontalRadius = Mathf.Clamp(armHorizontalRadius, -180, 180);
            // 上下旋转
            float verticalRotation = verticalInvert ? -Input.GetAxis("Mouse Y") : Input.GetAxis("Mouse Y");
            armVerticalRadius += verticalRotation * verticalSpeed;
            armVerticalRadius = Mathf.Clamp(armVerticalRadius, 0, 89);
            // 相机远近
            float distance = distanceInvert ? -Input.GetAxis("Mouse ScrollWheel") : Input.GetAxis("Mouse ScrollWheel");
            armLeghth += distance * distanceSpeed;
            armLeghth = Mathf.Clamp(armLeghth, 0, 10);
        }
    }
}
