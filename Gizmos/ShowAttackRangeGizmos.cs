using UnityEngine;

namespace RPGTest.Framework
{
    public class ShowAttackRangeGizmos : MonoBehaviour
    {
        [Header("位置")] public Transform centerTran;

        [Header("是否扇形")] public bool isSector = true;

        [Header("范围"), Range(1, 10)] public float radius = 1;
        [Header("角度/宽度"), Range(1, 360)] public float angleOrWidth = 1;
        [Header("高度"), Range(1, 10)] public float height = 1;
        [Header("间隔"), Range(1, 180)] public float horizontalInterval = 1;
        [Range(0.01f, 1)] public float verticalInterval = 0.5f;

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;

            if (!centerTran) centerTran = transform;

            Vector3 centerPos = centerTran.position;
            Vector3 currentCenterPos = centerPos;

            for (float i = centerPos.y; i <= centerPos.y + height; i += verticalInterval)
            {
                i = Mathf.Clamp(i, centerPos.y, centerPos.y + height);

                for (float j = -angleOrWidth * .5f; j <= angleOrWidth * .5f; j += horizontalInterval)
                {
                    j = Mathf.Clamp(j, -angleOrWidth * .5f, angleOrWidth * .5f);
                    if (isSector) Gizmos.DrawRay(currentCenterPos, Quaternion.AngleAxis(j, Vector3.up) * centerTran.forward * radius);
                    else
                    {
                        Vector3 tempCenterPos = currentCenterPos + transform.right * j/100;
                        Gizmos.DrawRay(tempCenterPos, centerTran.forward * radius);
                    }

                }
                currentCenterPos.y += verticalInterval;
            }
        }
    }
}