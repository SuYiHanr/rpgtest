using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorController : MonoBehaviour
{
    /// <summary>
    /// 攻击开始回调
    /// </summary>
    public event Action AttackStartCallBack;
    /// <summary>
    /// 攻击伤害回调
    /// </summary>
    public event Action AttackDamageCallBack;
    /// <summary>
    /// 攻击停止回调
    /// </summary>
    public event Action AttackStopCallBack;
    /// <summary>
    /// 被击开始回调
    /// </summary>
    public event Action GetHitStartCallBack;
    /// <summary>
    /// 被击结束回调
    /// </summary>
    public event Action GetHitStopCallBack;

    void Awake()
    {
        Animator anim = GetComponent<Animator>();
        // 通过动画名称添加动画关键帧
        foreach (var clip in anim.runtimeAnimatorController.animationClips)
        {
            // 攻击
            if (clip.name.Contains("Attack") && clip.events.Length < 3)
            {
                AnimationEvent _ae1 = new AnimationEvent();
                _ae1.functionName = "AttackStart";
                _ae1.time = 0;
                AnimationEvent _ae2 = new AnimationEvent();
                _ae2.functionName = "AttackDamage";
                _ae2.time = clip.length * .5f;
                AnimationEvent _ae3 = new AnimationEvent();
                _ae3.functionName = "AttackStop";
                _ae3.time = clip.length;
                clip.AddEvent(_ae1);
                clip.AddEvent(_ae2);
                clip.AddEvent(_ae3);

            }
            // 被击
            else if (clip.name.Contains("GetHit") && clip.events.Length < 2)
            {
                AnimationEvent _ae1 = new AnimationEvent();
                _ae1.functionName = "GetHitStart";
                _ae1.time = 0;
                AnimationEvent _ae2 = new AnimationEvent();
                _ae2.functionName = "GetHitStop";
                _ae2.time = clip.length;
                clip.AddEvent(_ae1);
                clip.AddEvent(_ae2);
            }
        }
    }

    void AttackStart()
    {
        AttackStartCallBack?.Invoke();
    }
    void AttackDamage()
    {
        AttackDamageCallBack?.Invoke();
    }
    void AttackStop()
    {
        AttackStopCallBack?.Invoke();
    }
    void GetHitStart()
    {
        GetHitStartCallBack?.Invoke();
    }
    void GetHitStop()
    {
        GetHitStopCallBack?.Invoke();
    }
}
