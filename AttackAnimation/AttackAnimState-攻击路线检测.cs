using System.Collections.Generic;
using UnityEngine;
using RPGTest.Character;

namespace RPGTest.Animation
{
    public class AttackAnimState : StateMachineBehaviour
    {
        /// <summary>
        /// 总帧数
        /// </summary>
        public float maxFrame;
        /// <summary>
        /// 攻击前摇
        /// </summary>
        public float beforAttackFrame;
        /// <summary>
        /// 攻击后摇
        /// </summary>
        public float LateAttackFrame;

        /// <summary>
        /// 攻击前摇时间
        /// </summary>
        float beforeAttackTime;
        /// <summary>
        /// 攻击后摇时间
        /// </summary>
        float LateAttackTime;
        /// <summary>
        /// 攻击计时器
        /// </summary>
        float attackTimer;

        /// <summary>
        /// 玩家类
        /// </summary>
        CharacterBase character;
        /// <summary>
        /// 攻击对象列表
        /// </summary>
        List<GameObject> hitList = new List<GameObject>();
        /// <summary>
        /// 动画基本播放时间
        /// </summary>
        float animBaseSpeed;

        /// <summary>
        /// 上一次攻击的点列表 根据攻击范围和间隔算出
        /// </summary>
        List<Vector3> lastAttackPoints = new List<Vector3>();
        /// <summary>
        /// 攻击点的个数
        /// </summary>
        int attackPointCount;
        /// <summary>
        /// 攻击点的间隔
        /// </summary>
        float attackPointInterval = .1f;
        /// <summary>
        /// 当前攻击点的间隔
        /// </summary>
        float currentAttackPointInterval;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            // 获取玩家类
            character = animator.transform.parent.GetComponent<CharacterBase>();
            // 设置攻击速度
            animBaseSpeed = animator.speed;
            animator.speed = character.dataSO.AttackSpeed;
            // 设置为攻击中
            character.onAttacking = true;
            // 攻击对象列表清零
            hitList.Clear();
            // 计算攻击前摇
            beforeAttackTime = beforAttackFrame / maxFrame * stateInfo.length / animator.speed;
            // 计算攻击后摇
            LateAttackTime = LateAttackFrame / maxFrame * stateInfo.length / animator.speed;
            // 重置攻击计时器
            attackTimer = 0;
            // 计算攻击点个数
            attackPointCount = Mathf.CeilToInt(character.dataSO.AttackRange / attackPointInterval);
            // 计算攻击点起始位置列表
            for (float i = 0; i <= attackPointCount; i++)
            {
                currentAttackPointInterval = i * attackPointInterval;
                currentAttackPointInterval = Mathf.Min(currentAttackPointInterval, character.dataSO.AttackRange);
                lastAttackPoints.Add(character.weapon.position + character.weapon.up * currentAttackPointInterval);
            }
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            // 攻击计时
            attackTimer += Time.deltaTime;
            // 计算攻击线条
            if (lastAttackPoints[0] != character.weapon.position)
            {
                for (int i = 0; i <= attackPointCount; i++)
                {
                    currentAttackPointInterval = i * attackPointInterval;
                    currentAttackPointInterval = Mathf.Min(currentAttackPointInterval, character.dataSO.AttackRange);
                    // 攻击前后摇不触发伤害
                    if (attackTimer >= beforeAttackTime && attackTimer <= LateAttackTime)
                    {
                        // 射线检测
                        RaycastHit raycastHit;
                        Physics.Linecast(lastAttackPoints[i], character.weapon.position + character.weapon.up * currentAttackPointInterval, out raycastHit);
                        // 如果检测到物体
                        if (raycastHit.collider)
                        {
                            GameObject target = raycastHit.collider.gameObject;
                            // 判断是否攻击过
                            if (!hitList.Contains(target))
                                // 判断是否是人类 且不能和自己一样
                                //TODO: if (!target.CompareTag(character.tag) && target.GetComponent<CharacterBase>())
                                if (!target.CompareTag(character.tag))
                                {
                                    // 敌人获得攻击 当前类创建伤害
                                    target.GetComponent<CharacterBase>().GetHit(character.DamageCalculations());
                                    // 添加到攻击列表
                                    hitList.Add(target);
                                }
                        }
#if UNITY_EDITOR_WIN
                        Debug.DrawLine(lastAttackPoints[i], character.weapon.position + character.weapon.up * currentAttackPointInterval, Color.green, 2f);
#endif
                    }
                    lastAttackPoints[i] = character.weapon.position + character.weapon.up * currentAttackPointInterval;
                }
            }




#if UNITY_EDITOR_WIN
            Debug.DrawRay(character.weapon.position, character.weapon.up * character.dataSO.AttackRange, Color.red, 2f);
#endif
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            // 取消攻击中
            character.onAttacking = false;
            // 还原攻击速度
            animator.speed = animBaseSpeed;
        }
    }
}