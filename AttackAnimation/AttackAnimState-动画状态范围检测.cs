using System.Collections.Generic;
using UnityEngine;
using RPGTest.Character;

namespace RPGTest.Animation
{
    public class AttackAnimState : StateMachineBehaviour
    {
        /// <summary>
        /// 总帧数
        /// </summary>
        public float maxFrame;
        /// <summary>
        /// 伤害帧
        /// </summary>
        public float damageFrame;

        /// <summary>
        /// 伤害时间
        /// </summary>
        float damageTime;
        /// <summary>
        /// 是否计算过伤害
        /// </summary>
        bool isDamaged;

        /// <summary>
        /// 玩家类
        /// </summary>
        CharacterBase character;
        /// <summary>
        /// 动画基本播放时间
        /// </summary>
        float animBaseSpeed;
        /// <summary>
        /// 基础角速度
        /// </summary>
        float baseAngularSpeed;
        /// <summary>
        /// 基础移动速度
        /// </summary>
        float baseMoveSpeed;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            // 获取玩家类
            character = animator.transform.parent.GetComponent<CharacterBase>();
            // 设置攻击速度
            animBaseSpeed = animator.speed;
            animator.speed = character.dataSO.AttackSpeed;
            // 减慢速度和角速度
            baseAngularSpeed = character.dataSO.AngularSpeed;
            baseMoveSpeed = character.dataSO.MoveSpeed;
            character.dataSO.AngularSpeed = baseAngularSpeed * 0.1f;
            character.dataSO.MoveSpeed = baseMoveSpeed * 0.1f;
            // 设置为攻击中
            character.onAttacking = true;
            // 计算伤害时间
            damageTime = damageFrame / maxFrame * stateInfo.length / animator.speed;
            // 重置伤害判断
            isDamaged = false;
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            // 攻击计时
            damageTime -= Time.deltaTime;
            // 被击期间和死亡期间不能攻击
            if (character.onBeHit || character.dataSO.CurrentHealth == 0) return;
            // if 没伤害过 且到伤害时间
            if (!isDamaged && damageTime <= 0)
            {
                // 避免多次攻击
                isDamaged = true;
                // 球形范围检测
                foreach (var collider in Physics.OverlapSphere(character.transform.position, character.dataSO.AttackRange))
                {
                    // 如果检测到敌人
                    if (!collider.CompareTag(character.tag) && collider.GetComponent<CharacterBase>())
                    {
                        // 获取敌人方位
                        Vector3 enemyDirection = (collider.transform.position - animator.transform.position).normalized;
                        // 判断角度 左右 60° 上 60°下 10°
                        if (Vector3.Dot(animator.transform.forward, enemyDirection) > 60 / 90 && (Vector3.Dot(animator.transform.up, enemyDirection) < 60 / 90 || Vector3.Dot(-animator.transform.up, enemyDirection) < 10 / 90))
                        {
                            // 敌人获得攻击 当前类创建伤害
                            collider.GetComponent<CharacterBase>().GetHit(character.DamageCalculations());
                        }
                    }
                }
            }
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            // 取消攻击中
            character.onAttacking = false;
            // 还原攻击速度
            animator.speed = animBaseSpeed;
            // 还原速度和角速度
            character.dataSO.AngularSpeed = baseAngularSpeed;
            character.dataSO.MoveSpeed = baseMoveSpeed;
        }
    }
}