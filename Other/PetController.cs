using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetController : MonoBehaviour
{
    /// <summary>
    /// 脖子位置
    /// </summary>
    public Transform neck;

    Animator animator;
    float currentSpeed, currentVelocity;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        // 退出程序
        if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();

        // 设置的屏幕分辨率为 300 * 500 可以计算出鼠标当前位置
        float x = Mathf.Clamp((Input.mousePosition.x - 300 * .5f) / Screen.width * .5f * 180, -40, 40);
        float y = Mathf.Clamp((Input.mousePosition.y - 500 * .5f) / Screen.height * .5f * 180, -25, 25);
        // 旋转脖子角度
        neck.transform.rotation = Quaternion.Euler(-y, -x, 0);

        // 获取当前速度
        currentSpeed = animator.GetFloat("Speed");
        // 按下鼠标右键移动
        if (Input.GetMouseButton(1)) animator.SetFloat("Speed", Mathf.SmoothDamp(currentSpeed, 1, ref currentVelocity, .2f));
        // 松开鼠标右键站立
        else animator.SetFloat("Speed", Mathf.SmoothDamp(currentSpeed, 0, ref currentVelocity, .2f));
    }
}
