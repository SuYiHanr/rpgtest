# RPGTest

## 1.简介
> Unity RPG 游戏开发学习

## 2.文件说明

### 1.AttackAnimation
> 攻击范围检测脚本

### 2.Camera
> 相机脚本

### 3.FiniteStateMachine
> 有限状态机脚本

### 4.Framework
> 框架脚本
1. 数值监听泛型类
2. 未完成

### 5.Gizmos
> 攻击范围显示脚本

### 6.InventorySystem
> 背包系统
1. 极简背包系
2. 未完成

### 7.Other
> 桌面宠物
1.隐藏窗口
2.背景透明
3.窗口拖动
4.窗口置顶